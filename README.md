接口文档生成工具

### 使用方法

####1、安装扩展

```
composer require x_mier/think-apidoc
```

####2、配置参数

- 安装好扩展后在 config\apidoc.php 配置文件

####3、书写规范

- 请参考 Demo.php 文件

####4、访问方法

- http://你的域名/doc 或者 http://你的域名/index.php/doc


使用方法

![alt text](image.png)